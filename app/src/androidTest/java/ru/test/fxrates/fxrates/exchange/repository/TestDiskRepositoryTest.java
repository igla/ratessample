package ru.test.fxrates.fxrates.exchange.repository;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Timed;
import ru.test.fxrates.exchange_screen.repository.DiskRepository;
import ru.test.fxrates.models.Rates;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by igor-lashkov on 03/07/2017.
 */

@RunWith(AndroidJUnit4.class)
public class TestDiskRepositoryTest {

    private Context mMockContext;

    @Before
    public void setUp() {
        mMockContext = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void test_save_rates_to_prefs() {
        Gson gson = new Gson();
        Rates rates = gson.fromJson("{\"base\":\"RUB\",\"date\":\"2017-06-30\",\"rates\":{\"AUD\":0.021987,\"BGN\":0.028956,\"BRL\":0.055667,\"CAD\":0.021889,\"CHF\":0.016182,\"CNY\":0.11457,\"CZK\":0.38785,\"DKK\":0.1101,\"GBP\":0.013018,\"HKD\":0.13186,\"HRK\":0.10971,\"HUF\":4.5743,\"IDR\":225.17,\"ILS\":0.059054,\"INR\":1.0918,\"JPY\":1.8913,\"KRW\":19.315,\"MXN\":0.30474,\"MYR\":0.072524,\"NOK\":0.1417,\"NZD\":0.023028,\"PHP\":0.85239,\"PLN\":0.062564,\"RON\":0.067397,\"SEK\":0.14272,\"SGD\":0.023259,\"THB\":0.5736,\"TRY\":0.059418,\"USD\":0.016895,\"ZAR\":0.22089,\"EUR\":0.014805}}", Rates.class);

        DiskRepository diskRepository = new DiskRepository(mMockContext);
        diskRepository.saveRates(new Timed<>(rates, 1, TimeUnit.MILLISECONDS));

        Observable<Timed<Rates>> pullRates = diskRepository.getRecentRates();
        assertNotNull(pullRates);
        TestObserver<Timed<Rates>> r = pullRates.test();
        r.awaitTerminalEvent();
        r
                .assertValueCount(1)
                .assertNoErrors()
                .assertValue(t -> t.value().getBase().equalsIgnoreCase("RUB"));
    }
}
