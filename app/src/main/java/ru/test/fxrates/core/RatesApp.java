package ru.test.fxrates.core;

import android.app.Application;

import ru.test.fxrates.dagger.AppModule;
import ru.test.fxrates.dagger.DaggerRateAppComponent;
import ru.test.fxrates.dagger.MainModule;
import ru.test.fxrates.dagger.RateAppComponent;

public class RatesApp extends Application {

    private RateAppComponent ratesComponent;
    private static RatesApp sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = RatesApp.this;
        ratesComponent = DaggerRateAppComponent.builder()
                .mainModule(new MainModule())
                .appModule(new AppModule(this))
                .build();
    }

    public RateAppComponent getComponent() {
        return ratesComponent;
    }

    public static synchronized RatesApp getInstance() {
        return sInstance;
    }
}
