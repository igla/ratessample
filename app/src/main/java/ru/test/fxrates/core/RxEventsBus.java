package ru.test.fxrates.core;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxEventsBus {

    private static RxEventsBus mInstance;

    private PublishSubject<Object> ratesEventSubject = PublishSubject.create();

    private RxEventsBus() {
    }

    public static RxEventsBus instanceOf() {
        if (mInstance == null) {
            mInstance = new RxEventsBus();
        }
        return mInstance;
    }

    public Observable<Object> getEventObservable() {
        return ratesEventSubject;
    }

    public void post(Object currencyCalculator) {
        ratesEventSubject.onNext(currencyCalculator);
    }
}