package ru.test.fxrates.events;

import ru.test.fxrates.exchange_screen.currency.CurrencyPagerAdapter;

/**
 * Created by igor-lashkov on 02/07/2017.
 */

public class MoneyValueChangeEvent {
    private
    @CurrencyPagerAdapter.FragmentType
    int fragmentType;

    public MoneyValueChangeEvent(@CurrencyPagerAdapter.FragmentType int fragmentType) {
        this.fragmentType = fragmentType;
    }

    public int getFragmentType() {
        return fragmentType;
    }
}
