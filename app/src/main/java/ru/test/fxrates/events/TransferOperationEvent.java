package ru.test.fxrates.events;

import ru.test.fxrates.models.TransferOperation;

/**
 * Created by igor-lashkov on 02/07/2017.
 */

public class TransferOperationEvent {
    private TransferOperation transferOperation;

    public TransferOperationEvent(TransferOperation transferOperation) {
        this.transferOperation = transferOperation;
    }

    public TransferOperation getTransferOperation() {
        return transferOperation;
    }
}
