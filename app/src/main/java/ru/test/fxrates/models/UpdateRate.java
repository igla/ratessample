package ru.test.fxrates.models;

/**
 * Created by igor-lashkov on 03/07/2017.
 */

public class UpdateRate {
    private TransferOperation transferOperation;
    private TransferDirection transferDirection;

    public UpdateRate(TransferOperation transferOperation, TransferDirection transferDirection) {
        this.transferOperation = transferOperation;
        this.transferDirection = transferDirection;
    }

    public TransferDirection getTransferDirection() {
        return transferDirection;
    }

    public TransferOperation getTransferOperation() {
        return transferOperation;
    }
}
