package ru.test.fxrates.models;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Created by igor-lashkov on 01/07/2017.
 */

public final class CurrencyCalculator implements ICurrencyCalculator {

    private static final List<String> CURRENCY_VALUES = Arrays.asList("USD", "EUR", "GBP");

    private final Rates rates;

    public CurrencyCalculator(@NonNull Rates rates) {
        this.rates = rates;
    }

    @Override
    public BigDecimal getRate(@NonNull TransferDirection transferDirection) {
        if (rates.getRates() == null) {
            return null;
        }
        String from = transferDirection.getCodeFrom();
        String to = transferDirection.getCodeTo();
        if (from.equalsIgnoreCase(to)) {
            return new BigDecimal(1);
        }
        if (from.equalsIgnoreCase(rates.getBase())) {
            return new BigDecimal(rates.getRates().get(to));
        }
        if (to.equalsIgnoreCase(rates.getBase())) {
            Double val = rates.getRates().get(from);
            return new BigDecimal(1).divide(new BigDecimal(val), 5, BigDecimal.ROUND_HALF_UP);
        }
        double convertRatio = 0;
        for (String s : CURRENCY_VALUES) {
            if (s.equalsIgnoreCase(rates.getBase())) {
                continue;
            }
            Double value = rates.getRates().get(s);
            if (value == null) {
                continue;
            }
            if (from.equalsIgnoreCase(s)) {
                convertRatio = rates.getRates().get(s);
                break;
            }
        }
        Double toValue = rates.getRates().get(to);
        if (toValue == null) {
            return new BigDecimal(0);
        }
        return new BigDecimal(toValue).divide(new BigDecimal(convertRatio), 5, BigDecimal.ROUND_HALF_UP);
    }
}
