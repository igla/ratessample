package ru.test.fxrates.models;

/**
 * Created by igor-lashkov on 03/07/2017.
 */

public class CurrencyOperation {
    private CurrencyItem itemFrom;
    private CurrencyItem itemTo;

    public CurrencyOperation(CurrencyItem itemFrom, CurrencyItem itemTo) {
        this.itemFrom = itemFrom;
        this.itemTo = itemTo;
    }

    public CurrencyItem getItemTo() {
        return itemTo;
    }

    public CurrencyItem getItemFrom() {
        return itemFrom;
    }
}
