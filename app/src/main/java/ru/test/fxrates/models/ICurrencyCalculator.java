package ru.test.fxrates.models;

import android.support.annotation.NonNull;

import java.math.BigDecimal;

/**
 * Created by igor-lashkov on 02/07/2017.
 */

public interface ICurrencyCalculator {
    BigDecimal getRate(@NonNull TransferDirection transferDirection);
}
