package ru.test.fxrates.models;

/**
 * Created by igor-lashkov on 02/07/2017.
 */

public class TransferOperation {
    private CurrencyItem currencyItemFrom;
    private CurrencyItem currencyItemTo;

    public TransferOperation(CurrencyItem currencyItemFrom, CurrencyItem currencyItemTo) {
        this.currencyItemFrom = currencyItemFrom;
        this.currencyItemTo = currencyItemTo;
    }

    public CurrencyItem getCurrencyItemTo() {
        return currencyItemTo;
    }

    public CurrencyItem getCurrencyItemFrom() {
        return currencyItemFrom;
    }
}
