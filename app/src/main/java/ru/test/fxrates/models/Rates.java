package ru.test.fxrates.models;

import android.support.annotation.Keep;

import java.util.HashMap;

/**
 * Created by igor-lashkov on 22/06/2017.
 */

@Keep
public final class Rates {
    private String base;
    private String date;
    private HashMap<String, Double> rates;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public HashMap<String, Double> getRates() {
        return rates;
    }

    public void setRates(HashMap<String, Double> rates) {
        this.rates = rates;
    }
}
