package ru.test.fxrates.models;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.Currency;
import java.util.Locale;

/**
 * Created by igor-lashkov on 01/07/2017.
 */

public final class TransferDirection {
    private final String from;
    private final String to;

    public TransferDirection(String codeFrom, String codeTo) {
        this.from = codeFrom;
        this.to = codeTo;
    }

    @Nullable
    private String getSymbol(String code) {
        switch (code) {
            case "USD":
                return Currency.getInstance("USD").getSymbol(Locale.US);
            case "EUR":
                return Currency.getInstance("EUR").getSymbol(Locale.FRANCE);
            case "GBP":
                return Currency.getInstance("GBP").getSymbol(Locale.UK);
        }
        return null;
    }

    public String getSymbolOrTitleFrom() {
        String symbol = getSymbol(from);
        return TextUtils.isEmpty(symbol) ? from : symbol;
    }

    public String getSymbolOrTitleTo() {
        String symbol = getSymbol(to);
        return TextUtils.isEmpty(symbol) ? to : symbol;
    }

    public String getCodeTo() {
        return to;
    }

    public String getCodeFrom() {
        return from;
    }

    public TransferDirection inverse() {
        return new TransferDirection(this.to, this.from);
    }
}
