package ru.test.fxrates.models;

import java.math.BigDecimal;

/**
 * Created by igor-lashkov on 02/07/2017.
 */

public final class CurrencyTitle {




    private final String base;
    private final BigDecimal value;

    public CurrencyTitle(String base, BigDecimal value) {
        this.base = base;
        this.value = value;
    }

    public String getBase() {
        return base;
    }

    public BigDecimal getValue() {
        return value;
    }
}
