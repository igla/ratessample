package ru.test.fxrates.dagger;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import ru.test.fxrates.core.RxEventsBus;
import ru.test.fxrates.exchange_screen.GetRatesInteractor;
import ru.test.fxrates.exchange_screen.repository.DiskRepository;
import ru.test.fxrates.exchange_screen.repository.DomainService;
import ru.test.fxrates.exchange_screen.repository.NetworkRepository;

@Module
public class MainModule {

    @Provides
    DomainService providesDomainService(DiskRepository diskRepository, NetworkRepository networkRepository) {
        return new DomainService(networkRepository, diskRepository);
    }

    @Provides
    GetRatesInteractor providesGetRatesInteractor(DomainService domainService) {
        return new GetRatesInteractor(domainService);
    }

    @Provides
    @Singleton
    DiskRepository providesDiskRepository(Application application) {
        return new DiskRepository(application);
    }

    @Provides
    @Singleton
    NetworkRepository providesNetworkRepository(Retrofit retrofit) {
        return new NetworkRepository(retrofit);
    }

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences(Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Singleton
    @Provides
    RxEventsBus providesEventBus() {
        return RxEventsBus.instanceOf();
    }
}