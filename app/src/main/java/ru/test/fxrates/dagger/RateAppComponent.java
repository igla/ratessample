package ru.test.fxrates.dagger;

import dagger.Component;

@Component(
        modules = {MainModule.class, AppModule.class, NetModule.class}
)
public interface RateAppComponent {
}