package ru.test.fxrates.exchange_screen;

import android.os.Bundle;

import ru.test.fxrates.R;
import ru.test.fxrates.core.base.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentContainer, new ExchangeFragment())
                    .commit();
        }
    }
}
