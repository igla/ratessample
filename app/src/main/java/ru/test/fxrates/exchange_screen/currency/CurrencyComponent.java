package ru.test.fxrates.exchange_screen.currency;

import javax.inject.Singleton;

import dagger.Component;
import ru.test.fxrates.dagger.AppModule;
import ru.test.fxrates.dagger.MainModule;
import ru.test.fxrates.dagger.RateAppComponent;

@Singleton
@Component(modules = {AppModule.class, MainModule.class},
        dependencies = RateAppComponent.class)
public interface CurrencyComponent {
    CurrencyPresenter presenter();
}