package ru.test.fxrates.exchange_screen;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public class EchangeViewState implements ViewState<ExchangeView> {

    final int STATE_SHOW_LOGIN_FORM = 0;
    final int STATE_SHOW_LOADING = 1;
    final int STATE_SHOW_ERROR = 2;

    int state = STATE_SHOW_LOGIN_FORM;

    @Override
    public void apply(ExchangeView view, boolean retained) {
        switch (state) {
            case STATE_SHOW_ERROR:
                view.showError();
                break;
        }
    }
}