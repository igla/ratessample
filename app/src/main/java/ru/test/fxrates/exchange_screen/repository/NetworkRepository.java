package ru.test.fxrates.exchange_screen.repository;


import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import ru.test.fxrates.models.Rates;
import ru.test.fxrates.network.RatesApi;

public class NetworkRepository {

    private final Retrofit retrofit;

    @Inject
    public NetworkRepository(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    Observable<Rates> getRecentRates() {
        RatesApi ratesApi = retrofit.create(RatesApi.class);
        return ratesApi.getRates("USD");
    }
}