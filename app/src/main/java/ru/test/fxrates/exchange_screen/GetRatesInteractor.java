package ru.test.fxrates.exchange_screen;

import android.support.annotation.WorkerThread;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import ru.test.fxrates.exchange_screen.repository.DomainService;
import ru.test.fxrates.models.CurrencyCalculator;
import ru.test.fxrates.models.Rates;

/**
 * Created by igor-lashkov on 29/06/2017.
 */
public class GetRatesInteractor implements IRateReceiveInteractor {

    private Disposable disposable;

    public DomainService domainService;

    @Inject
    public GetRatesInteractor(DomainService domainService) {
        this.domainService = domainService;
    }

    @WorkerThread
    @Override
    public void getRates(RateReceiveCallback callback) {
        requestCurrency(new Observer<CurrencyCalculator>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(CurrencyCalculator rates) {
                callback.onSuccess(rates);
            }

            @Override
            public void onError(Throwable e) {
                callback.onError();
            }

            @Override
            public void onComplete() {
            }
        });
    }

    private void requestCurrency(Observer<CurrencyCalculator> observer) {
        Observable<Rates> observable = domainService.getRecentRates();
        observable
                .map(CurrencyCalculator::new)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    void destroy() {
        if (disposable == null || disposable.isDisposed()) {
            return;
        }
        disposable.dispose();
    }
}
