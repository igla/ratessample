package ru.test.fxrates.exchange_screen;

import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.math.BigDecimal;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import ru.test.fxrates.core.RxEventsBus;
import ru.test.fxrates.events.MoneyValueChangeEvent;
import ru.test.fxrates.exchange_screen.currency.CurrencyPagerAdapter;
import ru.test.fxrates.models.CurrencyCalculator;
import ru.test.fxrates.models.CurrencyItem;
import ru.test.fxrates.models.CurrencyOperation;
import ru.test.fxrates.models.ICurrencyCalculator;
import ru.test.fxrates.models.TransferDirection;
import ru.test.fxrates.models.TransferOperation;
import ru.test.fxrates.models.UpdateRate;


class ExchangePresenter extends MvpBasePresenter<ExchangeView> {

    private CompositeDisposable compositeDisposable;

    private ICurrencyCalculator currencyCalculator;

    private GetRatesInteractor rateReceiveInteractor;

    private RxEventsBus eventsBus;

    @Inject
    public ExchangePresenter(RxEventsBus rxEventsBus, GetRatesInteractor getRatesInteractor) {
        this.eventsBus = rxEventsBus;
        this.rateReceiveInteractor = getRatesInteractor;
    }

    void resolveRates() {
        rateReceiveInteractor.getRates(new RateReceiveCallback() {
            @Override
            public void onError() {
                getView().showError();
            }

            @Override
            public void onSuccess(CurrencyCalculator rates) {
                onResolveRates(rates);
            }
        });
    }

    private void setRates(ICurrencyCalculator currencyCalculator) {
        this.currencyCalculator = currencyCalculator;
    }

    @Nullable
    BigDecimal calcRate(TransferDirection transferDirection, BigDecimal money) {
        if (currencyCalculator == null) {
            return null;
        }
        BigDecimal rate = currencyCalculator.getRate(transferDirection);
        return rate.multiply(money);
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            rateReceiveInteractor.destroy();
            getCompositeDisposable().dispose();
        }
    }

    @Override
    public void attachView(ExchangeView view) {
        super.attachView(view);
        Disposable disposable = eventsBus.getEventObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Object>() {

                    @Override
                    public void onNext(Object event) {
                        if (event instanceof MoneyValueChangeEvent) {
                            @CurrencyPagerAdapter.FragmentType int val = ((MoneyValueChangeEvent) event).getFragmentType();
                            onMoneyValueChange(val);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
        getCompositeDisposable().add(disposable);
    }

    private CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
        return compositeDisposable;
    }

    private void updateRates(boolean isChangedFrom) {
        CurrencyOperation currencyOperation = getView().getCurrencyOperation();
        if (currencyOperation == null) {
            return;
        }
        if (currencyCalculator == null) {
            return;
        }
        CurrencyItem itemFrom = currencyOperation.getItemFrom();
        CurrencyItem itemTo = currencyOperation.getItemTo();
        TransferDirection transferDirection = new TransferDirection(
                itemFrom.getBase(),
                itemTo.getBase()
        );
        String fromVal = String.valueOf(itemFrom.getValue());
        final BigDecimal bFromVal = new BigDecimal(fromVal);

        String toVal = String.valueOf(itemTo.getValue());
        final BigDecimal bToVal = new BigDecimal(toVal);

        final BigDecimal result;
        if (isChangedFrom) {
            result = calcRate(transferDirection, bFromVal);
        } else {
            result = calcRate(transferDirection.inverse(), bToVal);
        }
        if (result == null) {
            return;
        }

        BigDecimal from, to;
        if (itemFrom.getBase().equalsIgnoreCase(itemTo.getBase())) {
            from = isChangedFrom ? bFromVal : result;
            to = isChangedFrom ? result : bToVal;
        } else {
            from = isChangedFrom ? bFromVal : result;
            to = isChangedFrom ? result : bToVal;
        }

        TransferOperation transferOperation = new TransferOperation(
                new CurrencyItem(transferDirection.getCodeFrom(), from),
                new CurrencyItem(transferDirection.getCodeTo(), to)
        );
        getView().showRateData(new UpdateRate(transferOperation, transferDirection));
    }

    private void onMoneyValueChange(@CurrencyPagerAdapter.FragmentType int fragmentType) {
        updateRates(fragmentType == CurrencyPagerAdapter.FROM);
    }

    private void onResolveRates(CurrencyCalculator currencyCalculator) {
        setRates(currencyCalculator);
        updateRates(true);
    }

    void onSelectPage(@CurrencyPagerAdapter.FragmentType int fragmentType) {
        updateRates(fragmentType == CurrencyPagerAdapter.TO);
    }
}