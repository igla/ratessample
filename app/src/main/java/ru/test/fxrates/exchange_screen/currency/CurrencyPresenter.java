package ru.test.fxrates.exchange_screen.currency;

import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.math.BigDecimal;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import ru.test.fxrates.core.RxEventsBus;
import ru.test.fxrates.events.MoneyValueChangeEvent;
import ru.test.fxrates.events.TransferOperationEvent;
import ru.test.fxrates.models.CurrencyItem;
import ru.test.fxrates.models.TransferOperation;


class CurrencyPresenter extends MvpBasePresenter<CurrencyView> {

    private RxEventsBus eventBus;

    private CompositeDisposable compositeDisposable;

    @Inject
    public CurrencyPresenter(RxEventsBus eventBus) {
        this.eventBus = eventBus;
    }

    void onTextChange() {
        eventBus.post(new MoneyValueChangeEvent(getView().getFragmentType()));
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
        }
        getCompositeDisposable().dispose();
    }

    @Override
    public void attachView(CurrencyView view) {
        super.attachView(view);
        Disposable disposable = eventBus.getEventObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Object>() {

                    @Override
                    public void onNext(Object event) {
                        if (event instanceof TransferOperationEvent) {
                            TransferOperation transferOperation = ((TransferOperationEvent) event).getTransferOperation();
                            BigDecimal calcRate = getCalcRate(transferOperation);
                            if (calcRate == null) {
                                return;
                            }
                            if (calcRate.compareTo(BigDecimal.ZERO) == 0) {
                                getView().resetValue();
                            } else {
                                getView().showValue(calcRate);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
        getCompositeDisposable().add(disposable);
    }

    @Nullable
    private BigDecimal getCalcRate(TransferOperation transferOperation) {
        CurrencyItem itemFrom = transferOperation.getCurrencyItemFrom();
        CurrencyItem itemTo = transferOperation.getCurrencyItemTo();
        if (getView().getFragmentType() == CurrencyPagerAdapter.FROM) {
            if (itemFrom.getBase().equalsIgnoreCase(getView().getCurrencyBase())) {
                return itemFrom.getValue();
            }
        } else {
            if (itemTo.getBase().equalsIgnoreCase(getView().getCurrencyBase())) {
                return itemTo.getValue();
            }
        }
        return null;
    }

    private CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
        return compositeDisposable;
    }
}