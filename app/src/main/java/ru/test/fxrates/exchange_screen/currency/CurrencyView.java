package ru.test.fxrates.exchange_screen.currency;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.math.BigDecimal;

public interface CurrencyView extends MvpView {

    @CurrencyPagerAdapter.FragmentType
    int getFragmentType();

    String getCurrencyBase();

    void showValue(BigDecimal value);

    void resetValue();
}