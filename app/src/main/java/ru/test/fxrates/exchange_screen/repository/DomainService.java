package ru.test.fxrates.exchange_screen.repository;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.Timed;
import ru.test.fxrates.models.Rates;

public class DomainService {

    private static final int DELAY_IN_SEC = 30;

    final NetworkRepository networkRepository;
    final DiskRepository diskRepository;

    @Inject
    public DomainService(NetworkRepository networkRepository, DiskRepository diskRepository) {
        this.networkRepository = networkRepository;
        this.diskRepository = diskRepository;
    }

    public Observable<Rates> getRecentRates() {
        return getMergedData()
                .repeatWhen(o -> o.concatMap(v -> Observable.interval(0, DELAY_IN_SEC, TimeUnit.SECONDS)))
                .filter(getRecentRatesFilter())
                .map(Timed::value);
    }

    private Observable<Timed<Rates>> getMergedData() {
        return Observable.mergeDelayError( // <-- don't interrupt stream
                diskRepository.getRecentRates().subscribeOn(Schedulers.io()),
                networkRepository.getRecentRates().timestamp()
                        .filter(getRecentRatesFilter())
                        .doOnNext(diskRepository::saveRates)
                        .subscribeOn(Schedulers.io())
        );
    }

    private Predicate<Timed<Rates>> getRecentRatesFilter() {
        return ratesTimed -> ratesTimed != null
                && ratesTimed.value() != null
                && ratesTimed.value().getRates() != null;
    }
}