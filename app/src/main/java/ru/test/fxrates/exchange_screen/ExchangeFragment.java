package ru.test.fxrates.exchange_screen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rd.PageIndicatorView;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;
import ru.test.fxrates.R;
import ru.test.fxrates.core.RatesApp;
import ru.test.fxrates.core.RxEventsBus;
import ru.test.fxrates.core.base.BaseViewStateFragment;
import ru.test.fxrates.dagger.AppModule;
import ru.test.fxrates.dagger.NetModule;
import ru.test.fxrates.events.TransferOperationEvent;
import ru.test.fxrates.exchange_screen.currency.CurrencyFragment;
import ru.test.fxrates.exchange_screen.currency.CurrencyPagerAdapter;
import ru.test.fxrates.models.CurrencyItem;
import ru.test.fxrates.models.CurrencyOperation;
import ru.test.fxrates.models.TransferDirection;
import ru.test.fxrates.models.UpdateRate;

/**
 * Created by igor-lashkov on 28/06/2017.
 */

public class ExchangeFragment extends BaseViewStateFragment<ExchangeView, ExchangePresenter, EchangeViewState>
        implements ExchangeView {

    private static final String ENDPOINT = "http://api.fixer.io/";

    @BindView(R.id.viewpagerFrom)
    ViewPager viewPagerFrom;
    @BindView(R.id.viewpagerTo)
    ViewPager viewPagerTo;
    @BindView(R.id.pageIndicatorViewFrom)
    PageIndicatorView pageIndicatorViewFrom;
    @BindView(R.id.pageIndicatorViewTo)
    PageIndicatorView pageIndicatorViewTo;
    @BindView(R.id.rootContainer)
    LinearLayout rootContainer;
    @BindView(R.id.convertRate)
    TextView convertRate;

    CurrencyPagerAdapter pagerAdapterFrom;
    CurrencyPagerAdapter pagerAdapterTo;

    private ExchangeComponent exchangeComponent;

    CompositeDisposable compositeDisposable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_exchange;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pagerAdapterFrom = new CurrencyPagerAdapter(getFragmentManager(), CurrencyPagerAdapter.FROM);
        viewPagerFrom.setOffscreenPageLimit(3);
        viewPagerFrom.setAdapter(pagerAdapterFrom);
        viewPagerFrom.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                onSelectPage(CurrencyPagerAdapter.FROM);
            }
        });
        pageIndicatorViewFrom.setViewPager(viewPagerFrom);

        pagerAdapterTo = new CurrencyPagerAdapter(getFragmentManager(), CurrencyPagerAdapter.TO);
        viewPagerTo.setOffscreenPageLimit(3);
        viewPagerTo.setAdapter(pagerAdapterTo);
        viewPagerTo.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                onSelectPage(CurrencyPagerAdapter.TO);
            }
        });
        pageIndicatorViewTo.setViewPager(viewPagerTo);
    }

    private void onSelectPage(@CurrencyPagerAdapter.FragmentType int fragmentType) {
        presenter.onSelectPage(fragmentType);
    }

    @Override
    public CurrencyOperation getCurrencyOperation() {
        int from = viewPagerFrom.getCurrentItem();
        int to = viewPagerTo.getCurrentItem();
        CurrencyFragment fragmentFrom = (CurrencyFragment) pagerAdapterFrom.getFragment(from);
        CurrencyFragment fragmentTo = (CurrencyFragment) pagerAdapterTo.getFragment(to);
        if (fragmentFrom == null || fragmentTo == null) {
            return null;
        }
        CurrencyItem itemFrom = fragmentFrom.getCurrencyItem();
        CurrencyItem itemTo = fragmentTo.getCurrencyItem();
        return new CurrencyOperation(itemFrom, itemTo);
    }

    private void setRateCourse(TransferDirection transferDirection) {
        if (transferDirection.getCodeFrom().equalsIgnoreCase(transferDirection.getCodeTo())) {
            convertRate.setVisibility(View.INVISIBLE);
            return;
        }
        final BigDecimal value = presenter.calcRate(transferDirection, new BigDecimal(1));
        if (value == null) {
            convertRate.setVisibility(View.INVISIBLE);
            return;
        }
        convertRate.setVisibility(View.VISIBLE);

        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(5);
        nf.setMinimumFractionDigits(1);
        String resValue = nf.format(value);
        convertRate.setText(String.format(
                Locale.US,
                "%s1 = %s%s",
                transferDirection.getSymbolOrTitleFrom(),
                transferDirection.getSymbolOrTitleTo(),
                resValue
                )
        );
    }

    private CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
        return compositeDisposable;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getCompositeDisposable().dispose();
    }

    @NonNull
    @Override
    public EchangeViewState createViewState() {
        return new EchangeViewState();
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.resolveRates();
    }

    @NonNull
    @Override
    public ExchangePresenter createPresenter() {
        return exchangeComponent.presenter();
    }

    @Override
    public void showError() {
        Snackbar.make(rootContainer, "Error occured", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showRateData(UpdateRate updateRate) {
        setRateCourse(updateRate.getTransferDirection());
        RxEventsBus.instanceOf().post(new TransferOperationEvent(updateRate.getTransferOperation()));
    }

    @Override
    protected void injectDependencies() {
        exchangeComponent = DaggerExchangeComponent.builder()
                .appModule(new AppModule(RatesApp.getInstance()))
                .netModule(new NetModule(ENDPOINT))
                .rateAppComponent(RatesApp.getInstance().getComponent())
                .build();
    }
}
