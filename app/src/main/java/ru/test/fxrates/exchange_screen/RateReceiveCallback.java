package ru.test.fxrates.exchange_screen;

import ru.test.fxrates.models.CurrencyCalculator;

/**
 * Created by igor-lashkov on 28/06/2017.
 */

interface RateReceiveCallback {
    void onError();

    void onSuccess(CurrencyCalculator rates);
}
