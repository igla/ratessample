package ru.test.fxrates.exchange_screen;

import android.support.annotation.WorkerThread;

/**
 * Created by igor-lashkov on 29/06/2017.
 */

interface IRateReceiveInteractor {
    @WorkerThread
    void getRates(RateReceiveCallback callback);
}
