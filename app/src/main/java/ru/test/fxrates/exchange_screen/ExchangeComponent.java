package ru.test.fxrates.exchange_screen;

import javax.inject.Singleton;

import dagger.Component;
import ru.test.fxrates.dagger.AppModule;
import ru.test.fxrates.dagger.MainModule;
import ru.test.fxrates.dagger.NetModule;
import ru.test.fxrates.dagger.RateAppComponent;

@Singleton
@Component(modules = {AppModule.class, MainModule.class, NetModule.class},
        dependencies = RateAppComponent.class)
public interface ExchangeComponent {
    ExchangePresenter presenter();
}