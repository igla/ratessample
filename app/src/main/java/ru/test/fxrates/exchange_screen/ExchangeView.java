package ru.test.fxrates.exchange_screen;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import ru.test.fxrates.models.CurrencyOperation;
import ru.test.fxrates.models.UpdateRate;

public interface ExchangeView extends MvpView {

    CurrencyOperation getCurrencyOperation();

    void showError();

    void showRateData(UpdateRate transferOperation);
}