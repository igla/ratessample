package ru.test.fxrates.exchange_screen.currency;

import android.support.annotation.IntDef;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


public class CurrencyPagerAdapter extends FragmentPagerAdapter {

    @IntDef({FROM, TO})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FragmentType {
    }

    public final static int FROM = 1;
    public final static int TO = 2;

    private static final int TABS_NUM = 3;

    private SparseArray<Fragment> mPageReferenceMap = new SparseArray<>();

    private
    @FragmentType
    int fragmentType;

    public CurrencyPagerAdapter(FragmentManager fm, @FragmentType int fragmentType) {
        super(fm);
        this.fragmentType = fragmentType;
    }

    public Fragment getFragment(int key) {
        return mPageReferenceMap.get(key);
    }

    /**
     * After an orientation change, the fragments are saved in the adapter, and
     * I don't want to double save them: I will retrieve them and put them in my
     * list again here.
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container,
                position);
        mPageReferenceMap.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }

    @Override
    public int getCount() {
        return TABS_NUM;
    }

    @Override
    public Fragment getItem(int position) {
        String currency = null;
        switch (position) {
            case 0:
                currency = "GBP";
                break;
            case 1:
                currency = "USD";
                break;
            case 2:
                currency = "EUR";
                break;
        }
        if (currency == null) {
            return null;
        }
        CurrencyFragment currencyFragment = new CurrencyFragmentBuilder(
                currency,
                fragmentType
        ).build();
        mPageReferenceMap.put(position, currencyFragment);
        return currencyFragment;
    }
}