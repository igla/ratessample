package ru.test.fxrates.exchange_screen.currency;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;

import java.math.BigDecimal;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import ru.test.fxrates.R;
import ru.test.fxrates.core.RatesApp;
import ru.test.fxrates.core.base.BaseViewStateFragment;
import ru.test.fxrates.models.CurrencyItem;
import ru.test.fxrates.utils.Utils;

/**
 * Created by igor-lashkov on 28/06/2017.
 */

@FragmentWithArgs
public class CurrencyFragment extends BaseViewStateFragment<CurrencyView, CurrencyPresenter, CurrencyViewState>
        implements CurrencyView {

    boolean canListenTextChanges = true;

    @BindView(R.id.rootContainer)
    LinearLayout rootContainer;
    @BindView(R.id.currencyTitle)
    TextView currencyTitleFrom;
    @BindView(R.id.currencyValueFrom)
    EditText currencyValue;

    private CurrencyComponent loginComponent;

    CompositeDisposable compositeDisposable;

    @Arg
    @CurrencyPagerAdapter.FragmentType
    int fragmentType;
    @Arg
    String currencyBase;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (!isVisibleToUser && fragmentType == CurrencyPagerAdapter.FROM) {
                resetValue();
            }
        }
    }

    public CurrencyItem getCurrencyItem() {
        return new CurrencyItem(
                currencyBase,
                Utils.parseStrNumber(currencyValue.getText().toString())
        );
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_currency;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        currencyTitleFrom.setText(currencyBase);
        Disposable disposable =
                RxTextView.textChangeEvents(currencyValue)
                        .skipInitialValue()
                        .filter(changes -> !TextUtils.isEmpty(changes.text().toString()))
                        .filter(changes -> canListenTextChanges)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(getRateSearchFromObserver());
        getCompositeDisposable().add(disposable);
    }

    private CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
        return compositeDisposable;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getCompositeDisposable().dispose();
    }

    @NonNull
    @Override
    public CurrencyViewState createViewState() {
        return new CurrencyViewState();
    }

    @Override
    public void onNewViewStateInstance() {
    }

    @NonNull
    @Override
    public CurrencyPresenter createPresenter() {
        return loginComponent.presenter();
    }

    @Override
    public int getFragmentType() {
        return fragmentType;
    }

    @Override
    public String getCurrencyBase() {
        return currencyBase;
    }

    @Override
    public void showValue(BigDecimal value) {
        canListenTextChanges = false;
        currencyValue.setText(Utils.getNumberValue(value));
        canListenTextChanges = true;
    }

    @Override
    public void resetValue() {
        currencyValue.setText("");
    }

    @Override
    protected void injectDependencies() {
        loginComponent = DaggerCurrencyComponent.builder()
                .rateAppComponent(RatesApp.getInstance().getComponent())
                .build();
    }

    private DisposableObserver<TextViewTextChangeEvent> getRateSearchFromObserver() {
        return new DisposableObserver<TextViewTextChangeEvent>() {
            @Override
            public void onComplete() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(TextViewTextChangeEvent onTextChangeEvent) {
                presenter.onTextChange();
            }
        };
    }
}
