package ru.test.fxrates.exchange_screen.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Timed;
import ru.test.fxrates.models.Rates;

public class DiskRepository {

    private static final String CLASSNAME = DiskRepository.class.getCanonicalName();
    private final static String RECENT_RATES_RESPONSE_KEY = CLASSNAME + ".RecentRatesResponseKey";

    private final SharedPreferences sharedPreferences;

    @Inject
    public DiskRepository(Context context) {
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void saveRates(Timed<Rates> rates) {
        Gson gson = new Gson();
        String json = gson.toJson(rates.value());
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(RECENT_RATES_RESPONSE_KEY, json);
        prefsEditor.apply();
    }

    public Observable<Timed<Rates>> getRecentRates() {
        return Observable.fromCallable(() -> {
            String serializedRateList = sharedPreferences.getString(RECENT_RATES_RESPONSE_KEY, "");
            Rates rates = null;
            if (!TextUtils.isEmpty(serializedRateList)) {
                Gson gson = new Gson();
                rates = gson.fromJson(serializedRateList, Rates.class);
            }
            return new Timed<>(rates, System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        });
    }
}