package ru.test.fxrates.network;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


class LoggerInterceptor implements Interceptor {
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request originalRequest = chain.request();
        Log.i("HEADER", originalRequest.header("User-Agent"));
        Log.i("REQUEST INFO", originalRequest.toString());
        return chain.proceed(originalRequest);
    }
}