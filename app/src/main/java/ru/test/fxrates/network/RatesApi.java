package ru.test.fxrates.network;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.test.fxrates.models.Rates;


public interface RatesApi {
    @GET("/latest")
    Observable<Rates> getRates(@Query("base") String base);
}
