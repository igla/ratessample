package ru.test.fxrates.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;


public class OkHttpClientHelper {

    private static final int DEFAULT_TIMEOUT_IN_SEC = 5;

    private OkHttpClientHelper() {
    }

    private static class OkHttpClientHelperHolder {
        static final OkHttpClientHelper HOLDER_INSTANCE = new OkHttpClientHelper();
    }

    public static OkHttpClientHelper getInstance() {
        return OkHttpClientHelperHolder.HOLDER_INSTANCE;
    }

    public OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.readTimeout(DEFAULT_TIMEOUT_IN_SEC, TimeUnit.SECONDS);
        okHttpClient.writeTimeout(DEFAULT_TIMEOUT_IN_SEC, TimeUnit.SECONDS);
        okHttpClient.connectTimeout(DEFAULT_TIMEOUT_IN_SEC, TimeUnit.SECONDS);
        okHttpClient.retryOnConnectionFailure(true);
        okHttpClient.networkInterceptors().add(new LoggerInterceptor());
        okHttpClient.interceptors().add(new RetryInterceptor());
        return okHttpClient.build();
    }
}
