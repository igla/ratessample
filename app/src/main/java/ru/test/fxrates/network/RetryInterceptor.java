package ru.test.fxrates.network;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

class RetryInterceptor implements Interceptor {

    private static final int MAX_RETRY = 3;

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        int tryCount = 0;
        boolean shouldRetry = !response.isSuccessful() &&
                (response.code() < 400 || response.code() >= 500); //server error 5xx or redirect 3xx
        while (shouldRetry && tryCount < MAX_RETRY) {
            Log.d("intercept", "Request is not successful - " + tryCount);
            tryCount++;
            response = chain.proceed(request); // retry the request
        }
        return response; // otherwise just pass the original response on
    }
}