package ru.test.fxrates.utils;

import android.text.TextUtils;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 * Created by igor-lashkov on 02/07/2017.
 */

public final class Utils {
    private Utils() {
    }

    public static BigDecimal parseStrNumber(String strNumber) {
        if (TextUtils.isEmpty(strNumber)) {
            return BigDecimal.ZERO;
        }
        return parse(strNumber);
    }

    private static BigDecimal parse(String str) {
        String normalized = str.replaceAll("\\s", "").replace(',', '.');
        return new BigDecimal(normalized);
    }

    public static String getNumberValue(BigDecimal value) {
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(5);
        nf.setMinimumFractionDigits(0);
        return nf.format(value);
    }
}
