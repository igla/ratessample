package ru.test.fxrates;

import com.google.gson.Gson;

import org.junit.Test;

import java.math.BigDecimal;

import ru.test.fxrates.models.CurrencyCalculator;
import ru.test.fxrates.models.Rates;
import ru.test.fxrates.models.TransferDirection;

import static org.junit.Assert.assertEquals;


public class CurrencyCalculatorTest {
    @Test
    public void testGetRate() throws Exception {
        Gson gson = new Gson();
        Rates rates = gson.fromJson("{\"base\":\"RUB\",\"date\":\"2017-06-30\",\"rates\":{\"AUD\":0.021987,\"BGN\":0.028956,\"BRL\":0.055667,\"CAD\":0.021889,\"CHF\":0.016182,\"CNY\":0.11457,\"CZK\":0.38785,\"DKK\":0.1101,\"GBP\":0.013018,\"HKD\":0.13186,\"HRK\":0.10971,\"HUF\":4.5743,\"IDR\":225.17,\"ILS\":0.059054,\"INR\":1.0918,\"JPY\":1.8913,\"KRW\":19.315,\"MXN\":0.30474,\"MYR\":0.072524,\"NOK\":0.1417,\"NZD\":0.023028,\"PHP\":0.85239,\"PLN\":0.062564,\"RON\":0.067397,\"SEK\":0.14272,\"SGD\":0.023259,\"THB\":0.5736,\"TRY\":0.059418,\"USD\":0.016895,\"ZAR\":0.22089,\"EUR\":0.014805}}", Rates.class);
        CurrencyCalculator currencyCalculator = new CurrencyCalculator(rates);
        TransferDirection transferDirection = new TransferDirection("USD", "EUR");
        BigDecimal rate = currencyCalculator.getRate(transferDirection);
        assertEquals(new BigDecimal("0.87629"), rate);

        rate = currencyCalculator.getRate(transferDirection.inverse());
        assertEquals(new BigDecimal("1.14117"), rate);

        rate = currencyCalculator.getRate(new TransferDirection("USD", "USD"));
        assertEquals(new BigDecimal("1"), rate);
    }
}